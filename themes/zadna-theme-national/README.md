# Theme for the ZASchool project
Based on [hugo-serif-theme](https://github.com/zerostaticthemes/hugo-serif-theme)

Example of usage:
```
hugo new site zaschool-site
cd zaschool-site
git init 
git add .
git commit -m 'Initial commit'

git submodule add git@gitlab.com:zadna-schools/zadna-theme-national.git themes/zadna-theme-national

\cp -a themes/zadna-theme-national/exampleSite/* .
\cp -a themes/zadna-theme-national/exampleSite/.gitignore .
\cp -a themes/zadna-theme-national/exampleSite/.gitlab-ci.yml .

rm -f config.toml
git add .
git commit -m 'Add theme'

hugo server
```
Note the `\cp` to force the copy

To clone the sample site elsewhere, use `--recurse-submodules` to initialize and update the theme submodule
```
git clone --recurse-submodules git@gitlab.com:zadna-schools/zaschool-site.git
```